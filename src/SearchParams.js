import React, { useState } from "react";
import { ANIMALS } from "@frontendmasters/pet";
import useDropdown from "./useDropdown";

const SearchParams = () => {
  const [location, setLocation] = useState("Seattle, WA");
  const [breeds, setBreeds] = useState([]);
  const [animal, AnimalDropdown] = useDropdown("Animal", "Dog", ANIMALS);
  const [breed, BreedDropdown] = useDropdown("Breed", "", breeds);
  // const [animal, setAnimal] = useState("Dog");
  // const [breed, setBreed] = useState("");

  return (
    <div className="search-params">
      <form action="">
        <label htmlFor="location">Location</label>
        <input
          value={location}
          id="location"
          placeholder="location"
          onChange={e => setLocation(e.target.value)}
        />
        <AnimalDropdown />
        <BreedDropdown />
        <button>submit</button>
      </form>
    </div>
  );
};

export default SearchParams;
